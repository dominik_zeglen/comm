from socket import *
from thread import *
from time import sleep
from xml.dom import minidom

# Defining global constants
UDP_IP = ''
UP_PORT = 51320
DOWN_PORT = 52320
TIMEOUT = 10

# Defining global variables
threadList = []
userIPs = []
userNames = []
userTimes = []

# Defining Signal enumeration class
class Signal:
    ACK = 0
    PING = 1

# Defining Message Type enumeration class
class MessageType:
    INFO = 0
    MSG = 1
    LOGON = 2

# -----------------------------------------

# Packet composition
def composePacket(data, addr, type):
    # Main header and author
    msg = "<packet><author>" + userNames[userIPs.index(addr)][0] + "</author>"
    # Packet type - message / server info
    msg += "<type>" + str(type) + "</type>"
    # Message content
    msg += "<message>" + str(data) + "</message></packet>"

    return msg


# Server message packet composition
def composeServerMessagePacket(data):
    # Main header and author
    msg = "<packet><author>Server</author>"
    # Packet type - message / server info
    msg += "<type>" + str(MessageType.MSG) + "</type>"
    # Message content
    msg += "<message>" + str(data) + "</message></packet>"

    return msg


# Packet decomposition - XML parsing
def decomposePacket(data):
    tree = minidom.parseString(data)
    cNodes = tree.childNodes

    type = cNodes[0].getElementsByTagName("type")[0].childNodes[0].toxml()
    message = cNodes[0].getElementsByTagName("message")[0].childNodes[0].toxml()

    return message, int(type)


# Timeout procedure
def timeoutError(ind):
    global userIPs
    global userTimes
    global userNames

    print "Removing user #", ind, ": ", userNames[ind]

    userNames.pop(ind)
    userTimes.pop(ind)
    userIPs.pop(ind)


# Timeout checking
def checkTimeouts():
    global userTimes

    while True:
        for i in range(len(userTimes)):
            userTimes[i] -= 0.2
            if userTimes[i] < 0:
                timeoutError(i)

        sleep(0.2)


# Sending procedure
def sendTo(data, addr):
    print "Sending message to", addr
    sock = socket(AF_INET, SOCK_DGRAM)
    sock.sendto(data, addr)


# Multicast sender
def sendToAll(data):
    global userIPs

    for addr in userIPs:
        start_new_thread(sendTo, (data, addr))


# Listening procedure
def listener():
    global userIPs
    global userTimes
    global userNames

    sock = socket(AF_INET, SOCK_DGRAM)
    sock.bind((UDP_IP, DOWN_PORT))

    while True:
        try:
            data, addr = sock.recvfrom(128)
            print "Recv: ", data
            print "\tFrom: ", addr

            msg = decomposePacket(data)
            if msg[1] == MessageType.MSG:
                start_new_thread(sendToAll, (composePacket(msg[0], addr, MessageType.MSG), ))

            # Logon procedure
            elif msg[1] == MessageType.LOGON:
                userIPs.append(addr)
                userNames.append(msg[0])
                userTimes.append(TIMEOUT)
                start_new_thread(sendToAll, (composeServerMessagePacket("User {0} connected".format(msg[0])), ))

            # Keeping connection up
            elif msg[1] == MessageType.INFO:
                if int(msg[0]) == Signal.PING:
                    userTimes[userIPs.index(addr)] = TIMEOUT
                    start_new_thread(sendTo, (composePacket(Signal.ACK, addr, MessageType.INFO), addr))

                else:
                    start_new_thread(sendTo, (composePacket(Signal.ACK, addr, MessageType.INFO), addr))
                    print "Informational packet"
        except():
            print "No logged in user with this IP"

# Main function ---------------------------
start_new_thread(checkTimeouts, ())
listener()
