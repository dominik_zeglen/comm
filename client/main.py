from socket import *
from thread import *
from Tkinter import *
from time import sleep, time
from xml.dom import minidom
from unicodedata import normalize

# Defining global constants
UDP_IP = "92.222.73.23"
DOWN_PORT = 51320
UP_PORT = 52320
NUM_OF_LABELS = 11
MAX_CHARS = 45
TIMEOUT = 5


# Defining global variables
msgLog = []
threadList = []
timeoutCounter = 0
connected = False
sock = socket(AF_INET, SOCK_DGRAM)


# Defining Signal enumeration class
class Signal:
    ACK = 0
    PING = 1


# Defining Message Type enumeration class
class MessageType:
    INFO = 0
    MSG = 1
    LOGON = 2


# -----------------------------------------

# Packet decomposition - XML parsing
def decomposePacket(data):
    tree = minidom.parseString(data)
    cNodes = tree.childNodes
    author = cNodes[0].getElementsByTagName("author")[0].childNodes[0].toxml()
    type = cNodes[0].getElementsByTagName("type")[0].childNodes[0].toxml()
    message = cNodes[0].getElementsByTagName("message")[0].childNodes[0].toxml()

    return author, message, int(type)


def composePacket(data, type):
    # Main header
    msg = "<packet>"
    # Packet type - message / server info
    msg += "<type>" + str(type) + "</type>"
    # Message content
    msg += "<message>" + str(data) + "</message></packet>"

    return msg


# Sending to the server
def sendTo(msg):
    global sock
    sock.sendto(str(msg), (UDP_IP, UP_PORT))


# Server listener
def listener():
    global sock
    global msgLog
    global timeoutCounter

    sock.bind(('', DOWN_PORT))
    while True:
        data, addr = sock.recvfrom(256)
        msg = decomposePacket(data)
        timeoutCounter = TIMEOUT

        if msg[2] == MessageType.MSG:
            msgLog.append(msg)

        elif msg[2] == MessageType.INFO:
            pass


# Lower conversation container
class ResponseContainer(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master, padx=3, pady=3)
        self.pack(fill=X)
        self.createWidgets()

    def modifyCounter(self, var):
        t = len(var.get())
        if t > MAX_CHARS:
            self.counter["fg"] = "red"
        else:
            self.counter["fg"] = "black"
        self.varCounter.set(str(t) + " / " + str(MAX_CHARS))

    def sendMessage(self):
        if len(self.varEntry.get()) <= MAX_CHARS and len(self.varEntry.get()) > 0 and connected is True:
            sendTo(composePacket(normalize('NFKD', unicode(self.respBox.get())).encode('ascii', 'ignore'), MessageType.MSG))
            self.varEntry.set("")

    def returnHandler(self, event):
        self.sendMessage()

    def createWidgets(self):
        self.varCounter = StringVar()
        self.varEntry = StringVar()
        self.varEntry.trace("w", lambda name, index, mode, var=self.varEntry: self.modifyCounter(var))

        self.respBox = Entry(self)
        self.respBox["width"] = 50
        self.respBox["textvariable"] = self.varEntry
        self.respBox.pack(side="left")
        self.respBox.bind('<Return>', self.returnHandler)

        self.sendButton = Button(self)
        self.sendButton["text"] = "Send"
        self.sendButton["command"] = self.sendMessage
        self.sendButton["width"] = 5
        self.sendButton.pack(side="right")

        self.counter = Label(self)
        self.counter["textvariable"] = self.varCounter
        self.counter["width"] = 5
        self.counter["height"] = 2
        self.counter.pack(fill=X)


# Conversation text container
class ConversationTextContainer(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master, padx=3, pady=3)
        self.messages = ""
        self.currentView = 0
        self.pack(fill=X)
        self.createWidgets()

    def updateMessages(self):
        global msgLog

        try:
            if self.currentView > 0:
                t = self.currentView
            else:
                t = 0

            for i in range(NUM_OF_LABELS):
                self.varLabels[i].set(msgLog[i + t][0] + ":\t" + msgLog[i + t][1])
        except:
            pass

        t = 0
        if (len(msgLog) - self.currentView - NUM_OF_LABELS + 1) < 0:
            t = 0
        else:
            t = len(msgLog) - self.currentView - NUM_OF_LABELS + 1
        self.hiddenMessages.set(t)
        self.after(200, self.updateMessages)

    def goUp(self):
        if self.currentView > 0:
            self.currentView -= 1

    def goDown(self):
        if self.currentView < (len(msgLog) - NUM_OF_LABELS + 1):
            self.currentView += 1

    def createWidgets(self):
        self.convBox = Frame(self, padx=3, pady=3)
        self.varLabels = []
        self.hiddenMessages = StringVar()

        for i in range(NUM_OF_LABELS):
            self.varLabels.append(StringVar())

        self.convBoxLabels = []
        for i in range(NUM_OF_LABELS):
            self.convBoxLabels.append(Label(self.convBox, textvariable=self.varLabels[i], width=60, height=1))
            self.convBoxLabels[i]["anchor"] = "w"
            self.convBoxLabels[i].pack(side="top", fill=X)

        self.convBox.pack(side="left")

        self.buttonPanel = Frame(self, padx=3, pady=3)

        self.buttonUp = Button(self.buttonPanel)
        self.buttonUp["command"] = self.goUp
        self.buttonUp["width"] = 5
        self.buttonUp["height"] = 7
        self.buttonUp.pack(side="top")

        self.buttonDown = Button(self.buttonPanel)
        self.buttonDown["command"] = self.goDown
        self.buttonDown["width"] = 5
        self.buttonDown["height"] = 7
        self.buttonDown["textvariable"] = self.hiddenMessages
        self.buttonDown.pack(side="bottom")

        self.buttonPanel.pack(side="right", fill=Y)

        self.updateMessages()


# Logon window
class LogonWindow(Toplevel):
    def __init__(self, master = None):
        Toplevel.__init__(self, master)
        self.title("Please enter your name")

        self.label = Label(self, text = "Please enter your name")
        self.label.pack(side = "top")

        self.lowerContainer = Frame(self)
        self.lowerContainer.loginEntry = Entry(self.lowerContainer, width=10)
        self.lowerContainer.loginEntry.pack(side = "left")
        self.lowerContainer.loginButton = Button(self.lowerContainer, text="Login", command=self.login)
        self.lowerContainer.loginButton.pack(side = "right")
        self.lowerContainer.pack(side = "bottom")

        # Lift logon window above main window
        self.attributes("-topmost", True)

    def login(self):
        sendTo(composePacket(self.lowerContainer.loginEntry.get(), MessageType.LOGON))
        global timeoutCounter
        global connected

        timeoutCounter = 10

        # Wait for a 200 ms
        sleep(0.2)
        start_new_thread(keepConnection, ())
        connected = True

        self.destroy()


# Main conversation window
class ConversationWindow(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master, padx=3, pady=3)
        self.master.title("Multicast Communication")
        self.master.geometry("500x300")
        self.master.resizable(0, 0)
        self.pack(fill=X)

        # Logon window
        self.logon = LogonWindow(self)

        self.createWidgets()

    def createWidgets(self):
        self.textContainer = ConversationTextContainer(self)
        self.textContainer.pack(side="top")

        self.responseContainer = ResponseContainer(self)
        self.responseContainer.pack(side="bottom")


# Sending ping signal
def keepConnection():
    global timeoutCounter
    global connected

    while True:
        for i in range(int(TIMEOUT / 1)):
            timeoutCounter -= 1
            if i == 0:
                sendTo(composePacket(Signal.PING, MessageType.INFO))

            sleep(1)

        if timeoutCounter < 0:
            msgLog.append(['Localhost', 'Connection lost', MessageType.MSG])
            connected = False
            break


# Main function ---------------------------
try:
    start_new_thread(listener, ())
except:
    print "Could not start thread"

root = Tk()
app = ConversationWindow(root)
app.mainloop()

while True:
    try:
        root.update()
        sleep(1)
    except:
        print "Quitted"
        break
